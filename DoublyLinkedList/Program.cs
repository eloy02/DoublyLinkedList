﻿using DoublyLinkedList;using FileMode = System.IO.FileMode;

var list = new ListRandom();

list.Add("qwer");
list.Add("asdf");
list.Add("zxcv");
list.Add("poiu");
list.Add("lkjh");
list.Add("mnbv");

using (var fs = new FileStream("1.txt", FileMode.Create))
{
    list.Serialize(fs);
}

using (var fs = new FileStream("1.txt", FileMode.Open))
{
    var newList = new ListRandom();
    newList.Deserialize(fs);
    foreach (var item in newList)
    {
        Console.WriteLine($"Data: {item.Data}; Rand: {item.Random.Data}");
    }
}