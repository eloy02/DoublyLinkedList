using System.Collections;

namespace DoublyLinkedList;

public class ListRandom : IEnumerable<ListNode>
{
    public ListRandom()
    {
        Count = 0;
        Head = null;
        Tail = null;
    }
    
    public ListNode Head;
    public ListNode Tail;
    public int Count;

    public void Serialize(Stream s)
    {
        using var writer = new StreamWriter(s);

        var node = Head;
        for (var i = 0; i < Count; i++)
        {
            writer.WriteLine($"{node.Data};{IndexOf(node.Random)}");
            node = node.Next;
        }
    }

    public void Deserialize(Stream s)
    {
        using var reader = new StreamReader(s);
        
        var randsForRestore = new List<(int nodeIndex, int randIndex)>();
        
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            
            if(string.IsNullOrWhiteSpace(line))
                continue;
            
            var data = line.Split(";");
            var node = new ListNode()
            {
                Data = data[0],
                Random = IsEmpty() ? null : ElementAt(int.Parse(data[1]))
            };

            if (IsEmpty())
            {
                AddFirst(node);
            }
            else
            {
                AddNext(node, fillRandom: false);
            }

            if (node.Random is null)
            {
                randsForRestore.Add((Count - 1, int.Parse(data[1])));
            }
        }

        foreach (var (nodeIndex, randIndex) in randsForRestore)
        {
            var node = ElementAt(nodeIndex).Random = ElementAt(randIndex);
        }
    }

    public bool IsEmpty() => Head is null;

    public void Add(string value)
    {
        var node = new ListNode()
        {
            Data = value,
        };
        
        if (IsEmpty())
        {
            AddFirst(node);
        }
        else
        {
            AddNext(node);
        }

        // если всего 2 элемента, то у первого элемента случайным может быть только второй
        if (Count == 2)
        {
            Head.Random = Tail;
        }
    }

    public ListNode ElementAt(int index)
    {
        if (index >= Count || index < 0)
        {
            throw new ArgumentOutOfRangeException();
        }
        
        // todo: поиск с двух сторон
        var node = Head;
        for (var i = 0; i < index; i++)
        {
            node = node.Next;
        }
        
        return node;
    }
    
    public int IndexOf(ListNode item)
    {
        var index = 0;
        var currentItem = Head;
        while (currentItem != null)
        {
            if (currentItem.Equals(item))
            {
                return index;
            }
            index++;
            currentItem = currentItem.Next;
        }
        return -1;
    }

    public IEnumerator<ListNode> GetEnumerator()
    {
        var current = Head;
        while (current != null)
        {
            yield return current;
            current = current.Next;
        }
    }

    private void AddFirst(ListNode node)
    {
        Head = node;
        Tail = node;
        
        Count += 1;
    }

    private void AddNext(ListNode node, bool fillRandom = true)
    {
        node.Previous = Tail;
        node.Next = null;
        Tail.Next = node;
        Tail = node;
        
        Count += 1;

        if (!fillRandom)
        {
            return;
        }
        
        var rnd = new System.Random();
            
        node.Random = fillRandom ? ElementAt(rnd.Next(0, Count -1)) : null;
    }
    
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}